# For The Enclave

This repository is for my multi-hour long Fallout: New Vegas modification 'For The Enclave', which is available for download from its [NexusMods Page].

# Featureset

- The Enclave - Henderson Bunker Enclave forces, joinable by the player
- A fully fleshed-out main quest - The third resulting in a large scale assault and the fifth a dramatic and explosive conclusion to the story
- Side quests
- A companion
- Random encounters
- Various new and retextured armors, all with male & female models
- Three unique weapons
- Various new interiors, exteriors and a brand new world space
- A player-run Enclave bunker
- An Enclave Radio Station, courtesy of GCRust
- Fully lip-synced and voice-acted characters with all-new dialogue

# Development

I'm providing all source files here so that others may use my work as a resource to further their learning. As the mod is a .esm master file, feel free to make any addons using For The Enclave as a dependency and release as you wish without asking for my permission. I only ask that you do not re-upload these source files elsewhere, as they contain the work of others and I cannot grant permission for their distribution.

# Credits

### Devs and Resources
 - Otellino - Lead Designer, Interior & Exterior Cell design, AI, Scripting and QA
 - CNC - Designer, Scripter, NPC Design, Design advisor and QA
 - GCRust - Enclave Radio audio
 - Libby - Weapon and armor retextures
 - Speedy - Speedy Resources Modders Resource
 - c.i.b - Creature and Weapon Modders Resources for Subject X-76
 - Vivanto - Vertibird Interior Resource
 - Ruadhan2300 - TAR-3 Tesla Assault Rifle
 - Mikael Grizzly - Enclave Flight Jacket
### Voice Actors
 - Mr. K: General Jamison
 - Josef Boon - Operative Scully & Staff Sergeant Toby
 - Ross McKenzie - Enclave Messenger & Squad Commander
 - Charlie Wake - Chief Air Marshal Swanson & Henderson Bunker door guard
 - Robert Oakes - Staff Sergeant Collins
 - Shannon Hobby/Forceonature - Quartermaster Callaghan
 - Jezdamayel - Dr. Strangelove and Female Enclave soldiers
 - NDCowdy - Male Enclave soldiers
 - Crazyb2000 - Colonal Dornan Jr
 - MartinPurvis - Male Enclave soldiers & NCR Negotiator
 - forsamori - Male Enclave soldiers, Brotherhood of Steel diplomat & Subject X-76
 - Jude Rodney - General Rowlandson, Richardson Elite soldier
 - Garret - Mechanics
 - Ash Jack Creek - Operative Killian
 - Aranas - Lt. Jackson
 - Meg Foster - Enclave Officers
 - Edouard - Quartermaster & Enclave Officers
 - Rory - Male Enclave Officers
 - Tiffany Parker - Brotherhood Negotiator
 
[NexusMods Page]: <https://www.nexusmods.com/newvegas/mods/39531?>